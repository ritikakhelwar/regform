
let fetchBtn = document.getElementById("fetchBtn");
fetchBtn.addEventListener("click", buttonClickHandler)

function buttonClickHandler(){

     console.log("You've Clicked the fetchBtn")

     //instantiate a xhr object
     const xhr = new XMLHttpRequest();

     //open the object
     xhr.open('GET','https://jsonplaceholder.typicode.com/posts/1', true)

     //whats on progress(optional)
     xhr.onprogress = function () {
          console.log("On Progress..")
     }

     //what to do when the respose is ready
     xhr.onload = function () {
          if(this.status===200){
          console.log(this.responseText)
          }
          else{
               console.error("some error occured!")
          }
     }

     //send the request
     xhr.send();
}